package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.*;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String result = "";

        Class<?> clazz = Park.class;
        Constructor<?> constructor = clazz.getDeclaredConstructor();
        constructor.setAccessible(true);
        Object park = constructor.newInstance();
        System.out.println(park);
//        park = (Park) park;
        Field[] fields = park.getClass().getDeclaredFields();

        try (BufferedReader br = new BufferedReader(new FileReader(parkDatafilePath))) {
            String strCurrentLine;
            while ((strCurrentLine = br.readLine()) != null) {
                if (!strCurrentLine.equals("***")) {
                    String str = strCurrentLine.replace("\"", "");
                    String[] dataField = str.split(":");
                    String fieldName = dataField[0];
                    String fieldValue = dataField[1];
                    for (Field field : fields
                    ) {
                        String curField = field.getName();
                        FieldName fieldName1
                                = field.getAnnotation(
                                FieldName.class);
                        if (fieldName1 != null) {
                            curField = fieldName1.value();
                        }
                        if (curField.equals(fieldName)) {
                            MaxLength maxLength
                                    = field.getAnnotation(
                                    MaxLength.class);
                            int length = Integer.MAX_VALUE;
                            if (fieldName1 != null) {
                                length = maxLength.value();
                            }
                            NotBlank notBlank
                                    = field.getAnnotation(
                                    NotBlank.class);
                            boolean mayNull = true;
                            if (notBlank != null) {
                                mayNull = false;
                            }
                            if (fieldValue.length() < length) {
                                field.set(park, fieldValue);
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (Park) park;
    }
}
